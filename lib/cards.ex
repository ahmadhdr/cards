defmodule Cards do
  @moduledoc """
    Provides methods for creating and handling a deck of cards
  """

  @doc """
    Returns a list of string represinting a list of deck
  """
  def create_deck do
    values = ["Ace","Two","Three", "Four", "Five"]
    suits   = ["Spades", "Clubs", "Hearts", "Diamond","test"]
   for suit <- suits, value <- values do
        "#{value} of #{suit}"
    end
  end
  
  @doc """
    Shuffle given deck
  """
  def shuffle(deck) do
    Enum.shuffle(deck)
  end

  @doc """
    Determines wheter deck contains a given card
  ## Example
      iex> deck = Cards.create_deck
      iex> Cards.contains?(deck, "Ace of Spades")
      true
  """
  def contains?(deck, hand) do 
    Enum.member?(deck, hand) 
  end

  @doc """
    Divides a deck into a hand and the remainder of the deck.
    The `hand_size` argument indicates how many cars should
    be in the hand
  
  ## Examples
      iex> deck = Cards.create_deck
      iex> {hand, _deck} = Cards.deal(deck, 1)
      iex> hand
      ["Ace of Spades"]
  """
  def deal(deck, hand_size) do
    Enum.split(deck, hand_size)
  end
  
  @doc """
    Save deck to a file
  """
  def save(deck, filename) do 
    binary = :erlang.term_to_binary(deck)
    File.write(filename, binary)
  end

  @doc """
    Load saved deck from a file
  """
  def load(filename) do 
    case File.read(filename) do 
      {:ok, binary} -> :erlang.binary_to_term binary
      {:error, _reason} -> "That file does not exist"
    end
  end

  @doc """
    Shorcut for playing deck of a card
  """
  def create_hand(hand_size) do
    Cards.create_deck
    |> Cards.shuffle
    |> Cards.deal(hand_size)
  end
end
